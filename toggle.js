var ques = document.getElementsByClassName("question");

// Add onclick listener to all questions
for (var i = 0; i < ques.length; i++) {
  
  ques[i].onclick = function() {

    // Check if question is active
    var isActive = this.classList.contains("active");

    // Close all answers
    var allQues = document.getElementsByClassName("question");
    for (var j = 0; j < allQues.length; j++) {
       
      // Remove active class from section header
      allQues[j].classList.remove("active");

      // Close answer
      var ans = allQues[j].nextElementSibling;
      if (ans.style.maxHeight) {
        ans.style.maxHeight = null;
        ans.style.border = "0px"
      }
    }

    // Toggle the answer when question is clicked
    var ans = this.nextElementSibling;

    if (isActive) {
      this.classList.remove("active");
      ans.style.maxHeight = "0px";
      ans.style.border = "0px"
    }
    else {
      this.classList.add("active");
      ans.style.maxHeight = ans.scrollHeight + "px";
      ans.style.border = "2px solid black";
      ans.scrollIntoView();
    }
  };
}