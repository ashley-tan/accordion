module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            sitecss: {
                options: {
                    banner: '/* Minified CSS */'
                },
                files: {
                    'minified.css': [
                        'style.css'
                    ]
                }
            }
        },
        uglify: {
            options: {
                compress: true
            },
            applib: {
                src: [
                    'toggle.js'
                ],
                dest: 'uglified.js'
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    base: '.',
                    keepalive: true,
                    open: {
                        target: 'http://localhost:8000/accordion.html',
                        }
                    }
                }
            }
		});
        grunt.loadNpmTasks('grunt-contrib-connect');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
		grunt.registerTask('default');
    };